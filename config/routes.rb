RamusKlient::Application.routes.draw do
  
  resources :histories

  resources :kontaktni_osobas

  resources :zajem_o_informaces

  resources :leks

  resources :klient_leks

  resources :klients

  resources :osobas

  root "klients#index"

  get "/signin", to: "sessions#new", as: "signin"
  post "/signin", to: "sessions#create"
  get "/signout", to: "sessions#destroy", as: "signout"
  
  namespace :admin do
    root "base#index"
    resources :users
  end
    
end
