class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  
  before_action :set_current_user
  # before_action :set_last_seen_at, if: proc { |p| user_signed_in? }

  private
  
  # require sign in
  def require_signin!    
    if current_user.nil?
      redirect_to signin_url, alert: "Prosím přihlašte se ve formuláři níže"
    end
  end
  helper_method :require_signin!
  
  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end
  helper_method :current_user
  
  def set_last_seen_at
    current_user.update_attribute(:last_seen_at, Time.now)
  end


  def set_current_user
    User.current = current_user
  end

  def user_signed_in?
    current_user ? true : false    
  end
  helper_method :user_signed_in?
  
end