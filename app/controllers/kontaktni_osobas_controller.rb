class KontaktniOsobasController < ApplicationController
  before_action :set_kontaktni_osoba, only: [:show, :edit, :update, :destroy]

  # GET /kontaktni_osobas
  # GET /kontaktni_osobas.json
  def index
    @kontaktni_osobas = KontaktniOsoba.all
  end

  # GET /kontaktni_osobas/1
  # GET /kontaktni_osobas/1.json
  def show
  end

  # GET /kontaktni_osobas/new
  def new
    @kontaktni_osoba = KontaktniOsoba.new
    @klients = Klient.all
  end

  # GET /kontaktni_osobas/1/edit
  def edit
    @klients = Klient.all
  end

  # POST /kontaktni_osobas
  # POST /kontaktni_osobas.json
  def create
    @kontaktni_osoba = KontaktniOsoba.new(kontaktni_osoba_params)

    respond_to do |format|
      if @kontaktni_osoba.save
        format.html { redirect_to @kontaktni_osoba, notice: 'KontaktniOsoba was successfully created.' }
        format.json { render action: 'show', status: :created, location: @kontaktni_osoba }
      else
        format.html { render action: 'new' }
        format.json { render json: @kontaktni_osoba.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /kontaktni_osobas/1
  # PATCH/PUT /kontaktni_osobas/1.json
  def update
    respond_to do |format|
      if @kontaktni_osoba.update(kontaktni_osoba_params)
        format.html { redirect_to @kontaktni_osoba, notice: 'KontaktniOsoba was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @kontaktni_osoba.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /kontaktni_osobas/1
  # DELETE /kontaktni_osobas/1.json
  def destroy
    @kontaktni_osoba.destroy
    respond_to do |format|
      format.html { redirect_to kontaktni_osobas_url }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_kontaktni_osoba
    @kontaktni_osoba = KontaktniOsoba.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def kontaktni_osoba_params
    params.require(:kontaktni_osoba).permit(:name, :klient_ids => [])
  end
end
