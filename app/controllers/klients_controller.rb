class KlientsController < ApplicationController
  before_action :require_signin!
  before_action :set_klient, only: [:show, :edit, :update, :destroy]

  # GET /klients
  # GET /klients.json
  def index
    if current_user.admin? == false
      @klient = Klient.find_by osoba_id: current_user.osoba.id
      redirect_to action: 'edit', id:current_user.osoba.id
    elsif current_user.admin?
      @klients = Klient.all
    else
      redirect_to(signin_path)
    end
  end

  # GET /klients/1
  # GET /klients/1.json
  def show
    if current_user.admin?
      @kontaktni_osobas = KontaktniOsoba.all
    else
      redirect_to(root_path)
    end

    if @klient.vozickar
      @vozickar = "\u2713"
    else
      @vozickar = "\u2A2F"
    end

    if @klient.epilepsie
      @epilepsie = "\u2713"
    else
      @epilepsie = "\u2A2F"
    end

    if @klient.inkontinence
      @inkontinence = "\u2713"
    else
      @inkontinence = "\u2A2F"
    end


    # checking plnoletost
    rc = @klient.rodne_cislo[0,6]
    rc = rc[0,2].to_s + ',' + rc[2,2].to_s + ',' + rc[4,2].to_s

    # datum_narozeni = DateTime.strptime('{rc}','{%y%m%d}')

    if @klient.rodne_cislo[1,6].to_i < (Time.now.strftime('%Y%m%d').to_i)
      @plnoletost = "\u2713"
    else
      @plnoletost = "\u2A2F"
    end

    @plnoletost = 'TBI'

    if @klient.elektronicky_souhlas_s_uchovanim_udaju
      @elektronicky_souhlas_s_uchovanim_udaju = "\u2713"
    else
      @elektronicky_souhlas_s_uchovanim_udaju = "\u2A2F"
    end

    if @klient.souhlas_s_uverejnenim_foto_video
      @souhlas_s_uverejnenim_foto_video = "\u2713"
    else
      @souhlas_s_uverejnenim_foto_video = "\u2A2F"
    end

    if @klient.platny_zaznam
      @platny_zaznam = "\u2713"
    else
      @platny_zaznam = "\u2A2F"
    end

    @adresa = @klient.osoba.ulice.to_s + ' ' + @klient.osoba.cislo_popisne.to_s + 
    '/' + @klient.osoba.cislo_orientacni.to_s + ',' + 
    @klient.osoba.mesto.to_s + ' ' + @klient.osoba.psc.to_s
  end

  # GET /klients/new
  def new
    @klient = Klient.new
    @klient.build_osoba()
    # @kontaktni_osobas = KontaktniOsoba.all
  end

  # GET /klients/1/edit
  def edit
    # @kontaktni_osobas = KontaktniOsoba.all
    # if current_user.admin?
    #   redirect_to action: 'index'
    # end
  end

  # POST /klients
  # POST /klients.json
  def create
    @klient = Klient.new(klient_params)

    respond_to do |format|
      if @klient.save
        format.html { redirect_to @klient, notice: 'Klient was successfully created.' }
        format.json { render action: 'show', status: :created, location: @klient }
      else
        format.html { render action: 'new' }
        format.json { render json: @klient.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /klients/1
  # PATCH/PUT /klients/1.json
  def update
    respond_to do |format|
      if @klient.update(klient_params)
        format.html { redirect_to @klient, notice: 'Klient was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @klient.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /klients/1
  # DELETE /klients/1.json
  def destroy
    @klient.destroy
    respond_to do |format|
      format.html { redirect_to klients_url }
      format.json { head :no_content }
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_klient
      if current_user.admin?
        @klient = Klient.find(params[:id])    
      else
        @klient = Klient.find_by osoba_id: current_user.osoba.id
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def klient_params
      params.require(:klient).permit(:osoba_id, :rodne_cislo, :pojistovna, :vozickar, :epilepsie,
       :podoba_epileptickeho_zachvatu, :prukaz, :alergie, :inkontinence,
       :popis_projevu_diagnozy_a_instrukce_k_zachazeni_s_klientem, :denni_rezim,
       :plnoletost, :postrehy_ramus, :sklony_k_oddelovani_se_od_skupiny,
       :elektronicky_souhlas_s_uchovanim_udaju, :souhlas_s_uverejnenim_foto_video,
       :diagnoza, :platny_zaznam,
       osoba_attributes:[:id, :kontaktni_osoba_id, :jmeno, :prijmeni, :email, :telefon, :ulice, :cislo_popisne,
        :cislo_orientacni, :psc, :mesto],
        kontaktni_osobas_attributes:[:id, :osoba_id, :klient_id, :vztah_ke_klientovi, :jmeno, :prijmeni, :email,
         :telefon, :ulice, :cislo_popisne, :cislo_orientacni, :psc, :mesto, :_destroy],
         :kontaktni_osobas_ids => [],
         klient_leks_attributes:[:id, :klient_id, :lek_id, :davkovani],
         leks_attributes:[:id, :klient_id, :nazev, :pribalovy_letak, :davkovani, :_destroy],
         zajem_o_informace_attributes:[:id, :klient_id, :jemne_vikendovky, :drsne_vikendovky, :humr, :rambar, :dilna,
           :legendarni_tabory, :drsne_tabory, :_destroy])
end
end
