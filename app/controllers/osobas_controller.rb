class OsobasController < ApplicationController
    before_action :require_signin!, except: [:index]
    before_action :set_osoba, only: [:show, :edit, :update, :destroy]

    # GET /osobas
    # GET /osobas.json
    def index
      if current_user
        @osoba = current_user.osoba.id
        @osobas = Osoba.all
      else
        redirect_to(signin_path)
      end
    end

    # GET /osobas/1
    # GET /osobas/1.json
    def show
    end

    # GET /osobas/new
    def new
      @osoba = Osoba.new
      @osoba.build_klient
      # @kontaktni_osobas = KontaktniOsoba.all
    end

    # GET /osobas/1/edit
    def edit
      # @kontaktni_osobas = KontaktniOsoba.all
    end

    # POST /osobas
    # POST /osobas.json
    def create
      @osoba = Osoba.new(osoba_params)

      respond_to do |format|
        if @osoba.save
          format.html { redirect_to @osoba, notice: 'Osoba was successfully created.' }
          format.json { render action: 'show', status: :created, location: @osoba }
        else
          format.html { render action: 'new' }
          format.json { render json: @osoba.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /osobas/1
    # PATCH/PUT /osobas/1.json
    def update
      respond_to do |format|
        if @osoba.update(osoba_params)
          format.html { redirect_to @osoba, notice: 'Osoba was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render action: 'edit' }
          format.json { render json: @osoba.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /osobas/1
    # DELETE /osobas/1.json
    def destroy
      @osoba.destroy
      respond_to do |format|
        format.html { redirect_to osobas_url }
        format.json { head :no_content }
      end
    end


    private
    # Use callbacks to share common setup or constraints between actions.
    def set_osoba
      @osoba = Osoba.find(params[:id])
      # @osoba = Osoba.find_by osoba_id: current_user.osoba.id
    end

    # Never trust parameters from the scary internet, only allow the white list through.
  def osoba_params
    params.require(:osoba).permit(
      :id, 
      :jmeno, 
      :prijmeni, 
      :email, 
      :telefon,
      :ulice, 
      :cislo_popisne, 
      :cislo_orientacni, 
      :psc, 
      :mesto, 
      :_destroy,
      klient_attributes:[:id,:osoba_id,:rodne_cislo,:_destroy]
    )
  end
end
