class LeksController < ApplicationController
  before_action :require_signin!
  before_action :set_lek, only: [:show, :edit, :update, :destroy]

  # GET /leks
  # GET /leks.json

  # GET /leks/1
  # GET /leks/1.json
  def show
  end

  # GET /leks/new
  def new
    @lek = Lek.new
    # @kontaktni_osobas = KontaktniOsoba.all
  end

  # GET /leks/1/edit
  def edit
    # @kontaktni_osobas = KontaktniOsoba.all
  end

  # POST /leks
  # POST /leks.json
  def create
    @lek = Lek.new(lek_params)

    respond_to do |format|
      if @lek.save
        format.html { redirect_to @lek, notice: 'Lek was successfully created.' }
        format.json { render action: 'show', status: :created, location: @lek }
      else
        format.html { render action: 'new' }
        format.json { render json: @lek.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /leks/1
  # PATCH/PUT /leks/1.json
  def update
    respond_to do |format|
      if @lek.update(lek_params)
        format.html { redirect_to @lek, notice: 'Lek was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @lek.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /leks/1
  # DELETE /leks/1.json
  def destroy
    @lek.destroy
    respond_to do |format|
      format.html { redirect_to leks_url }
      format.json { head :no_content }
    end
  end


  private
  # Use callbacks to share common setup or constraints between actions.
  def set_lek
    @lek = Lek.find(params[:id])
    # @lek = Lek.find_by osoba_id: current_user.osoba.id
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def lek_params
    params.require(:lek).permit(:id, :nazev, :pribalovy_letak, :_destroy, klient_leks_attributes:[:id, :_destroy,
    :davkovani_rano, :davkovani_poledne, :davkovani_vecer, :davkovani_pred_spanim])
  end
end
