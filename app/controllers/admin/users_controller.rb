class Admin::UsersController < Admin::BaseController
  before_action :set_user, only:[
    :edit,
    :update
  ]

  def new
    @user = User.new
    @user.build_osoba
    @user.osoba.build_klient
    @user.osoba.klient.build_zajem_o_informace(jemne_vikendovky: false)
  end

  def create
        @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to root_path, notice: 'User was successfully created.' }
        format.json { render action: 'show', status: :created, location: @user }
      else
        format.html { render action: 'new' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  
  def edit    
  end
  
  def update
    new_params = user_params.dup
    new_params[:username] = new_params[:username].strip
    new_params[:email] = new_params[:email].strip
    if @user.update(new_params)
      redirect_to admin_root_path, notice: "Your account was successfully updated."
    else
      flash[:alert] = "Account not updated."
      render :edit
    end
  end
  
  
  private 
  
  def set_user
    @user = User.friendly.find(current_user.id)
  end
  
  def user_params
    params.require(:user).permit(
      :username,
      :email,
      :password,
      :password_confirmation,
      :osoba_id,
      osoba_attributes: [:id, :kontaktni_osoba_id, :jmeno, :prijmeni, :email, :telefon,
        :ulice, :cislo_popisne, :cislo_orientacni, :psc, :mesto, :_destroy, 
        klient_attributes: [:id, :osoba_id, :rodne_cislo, :pojistovna, :vozickar,
          :epilepsie, :podoba_epileptickeho_zachvatu, :alergie, :inkontinence, 
          :popis_projevu_diagnozy_a_instrukce_k_zachazeni_s_klientem, :denni_rezim,
          :plnoletost, :postrehy_ramus, :sklony_k_oddelovani_se_od_skupiny,
          :elektronicky_souhlas_s_uchovanim_udaju, :souhlas_s_uverejnenim_foto_video,
          :diagnoza, :platny_zaznam, :prukaz, :_destroy,
          zajem_o_informace_attributes: [:id, :klient_id, :jemne_vikendovky, 
            :drsne_vikendovky, :humr, :legendarni_tabory, :drsne_tabory,
            :rambar, :dilna, :_destroy]
            ]]
    )
  end
  
end
