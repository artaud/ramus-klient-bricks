class SessionsController < ApplicationController
  def new
    redirect_to(root_path, notice: "Jste již přihlášeni.") if current_user 
  end
  
  def create
    user = User.where(username: params[:signin][:username].strip).first

    if user && user.authenticate(params[:signin][:password])
    
      session[:user_id] = user.id 
      redirect_to root_path, notice: "Přihlášení proběhlo úspěšně."
    else
      flash[:error] = "Nesprávné jméno nebo heslo."
      render :new
    end
  end
  
  def destroy
    session[:user_id] = nil
    redirect_to root_url, notice: "Odhlášeno."
  end
  
end
