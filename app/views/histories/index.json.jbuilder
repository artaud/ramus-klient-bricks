json.array!(@histories) do |history|
  json.extract! history, :id, :object_name, :object_id, :attribute_name, :before_value, :after_value, :user_id
  json.url history_url(history, format: :json)
end
