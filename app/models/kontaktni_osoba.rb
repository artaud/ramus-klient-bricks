class KontaktniOsoba < ActiveRecord::Base
	include ActiveModel::Dirty

	after_create :write_history
	before_update :write_history

    # belongs_to :osoba
    has_one :osoba

    accepts_nested_attributes_for :osoba, reject_if: :all_blank, allow_destroy: true

  # has_many :klients_kontaktni_osobas
  # has_many :klients, through: :klients_kontaktni_osobas
  #
  # accepts_nested_attributes_for :klients, :klients_kontaktni_osobas

  private
  def write_history
  	self.changes.each do |attribute_name, values|
  		before_value = values[0].to_s.truncate(700) if !values[0].nil?
  		after_value = values[1].to_s.truncate(700) if !values[1].nil?
  		user_id = User.current.id
  		History.create!({:object_name => self.class.name,
  			:object_id => self.id,
  			:attribute_name => attribute_name,
  			:before_value => before_value.to_s,
  			:after_value => after_value.to_s,
  			:user_id => user_id})
  	end
  end

end
