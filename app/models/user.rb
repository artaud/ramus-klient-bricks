class User < ActiveRecord::Base
  belongs_to :osoba
  has_one :klient, :through => :osoba

  # Use friendly_id on Users
  extend FriendlyId
  friendly_id :friendify, use: :slugged
  
  # necessary to override friendly_id reserved words
  def friendify
    if username.downcase == "admin"
      "user-#{username}"
    else
      "#{username}"
    end
  end

  # to be able to access current_user in models
  def self.current
    Thread.current[:user]
  end

  def self.current=(user)
    Thread.current[:user] = user
  end

  # Use a secure password
  has_secure_password
  
  # Basic validations
  validates :username, presence: true
  validates :username, length: { in: 4..20 }
  validates :username, uniqueness: { case_sensitive: false }
  validates :email, uniqueness: { case_sensitive: false }, allow_nil: true, allow_blank: true
  validates :password, length: { minimum: 4 }, on: :create
  # validates_format_of :email, with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, on: :create
  validates_format_of :username, with: /\A[a-zA-Z0-9]*\z/, on: :create, message: "can only contain letters and digits"
  
  accepts_nested_attributes_for :osoba, :klient
end