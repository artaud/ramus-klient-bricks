class CreateKontaktniOsobas < ActiveRecord::Migration
  def change
    create_table :kontaktni_osobas do |t|
      t.integer :osobaid
      t.integer :klientosobaid
      t.string :vztah_ke_klientovi

      t.timestamps
    end
  end
end
