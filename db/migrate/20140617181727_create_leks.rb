class CreateLeks < ActiveRecord::Migration
  def change
    create_table :leks do |t|
      t.string :nazev
      t.text :pribalovy_letak

      t.timestamps
    end
  end
end
