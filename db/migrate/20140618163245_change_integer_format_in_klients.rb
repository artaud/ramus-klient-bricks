class ChangeIntegerFormatInKlients < ActiveRecord::Migration
  def change
    change_column :klients, :rodne_cislo, :string
  end
end
