class CreateZajemOInformaces < ActiveRecord::Migration
  def change
    create_table :zajem_o_informaces do |t|
      t.integer :klientosobaid
      t.boolean :jemne_vikendovky
      t.boolean :drsne_vikendovky
      t.boolean :humr
      t.boolean :legendarni_tabory
      t.boolean :drsne_tabory
      t.boolean :rambar
      t.boolean :dilna

      t.timestamps
    end
  end
end
