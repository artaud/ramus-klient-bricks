class ChangeIdsToIds < ActiveRecord::Migration
  def change
    rename_column :klients, :osobaid, :osoba_id
    rename_column :kontaktni_osobas, :osobaid, :osoba_id
    rename_column :kontaktni_osobas, :klientosobaid, :klient_id
    rename_column :users, :osobaid, :osoba_id
    rename_column :klient_leks, :klientosobaid, :klient_id
    rename_column :klient_leks, :lekid, :lek_id
    rename_column :zajem_o_informaces, :klientosobaid, :klient_id
  end
end
