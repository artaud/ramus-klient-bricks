class CreateKlientsKontaktniOsobas < ActiveRecord::Migration
  def change
    # drop_table :klients_kontaktni_osobas

    create_table :klients_kontaktni_osobas, id: false do |t|
      t.belongs_to :klient
      t.belongs_to :kontaktni_osoba

      t.timestamps
    end
  end
end
