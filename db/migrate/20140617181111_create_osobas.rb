class CreateOsobas < ActiveRecord::Migration
  def change
    create_table :osobas do |t|
      t.string :jmeno
      t.string :prijmeni
      t.string :email
      t.integer :telefon
      t.string :ulice
      t.integer :cislo_popisne
      t.integer :cislo_orientacni
      t.integer :psc
      t.string :mesto

      t.timestamps
    end
  end
end
