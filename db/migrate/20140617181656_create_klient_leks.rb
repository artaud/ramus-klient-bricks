class CreateKlientLeks < ActiveRecord::Migration
  def change
    create_table :klient_leks do |t|
      t.integer :klientosobaid
      t.integer :lekid
      t.float :davkovani_rano
      t.float :davkovani_poledne
      t.float :davkovani_vecer
      t.float :davkovani_pred_spanim

      t.timestamps
    end
  end
end
