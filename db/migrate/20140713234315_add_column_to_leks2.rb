class AddColumnToLeks2 < ActiveRecord::Migration
  def change
    add_column :leks, :davkovani, :text
    remove_column :leks, :davkovani_rano
    remove_column :leks, :davkovani_poledne
    remove_column :leks, :davkovani_vecer
    remove_column :leks, :davkovani_pred_spanim
  end
end
