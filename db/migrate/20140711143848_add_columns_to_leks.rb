class AddColumnsToLeks < ActiveRecord::Migration
  def change
    add_column :leks, :davkovani_rano, :string
    add_column :leks, :davkovani_poledne, :string
    add_column :leks, :davkovani_vecer, :string
    add_column :leks, :davkovani_pred_spanim, :string
  end
end
