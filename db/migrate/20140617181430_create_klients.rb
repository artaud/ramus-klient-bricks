class CreateKlients < ActiveRecord::Migration
  def change
    create_table :klients do |t|
      t.integer :osobaid
      t.integer :rodne_cislo
      t.integer :pojistovna
      t.boolean :vozickar
      t.boolean :epilepsie
      t.text :podoba_epileptickeho_zachvatu
      t.boolean :ztp
      t.boolean :ztp_p
      t.text :alergie
      t.boolean :inkontinence
      t.text :popis_projevu_diagnozy_a_instrukce_k_zachazeni_s_klientem
      t.text :denni_rezim
      t.boolean :plnoletost
      t.text :postrehy_ramus
      t.boolean :sklony_k_oddelovani_se_od_skupiny
      t.boolean :elektronicky_souhlas_s_uchovanim_udaju
      t.boolean :souhlas_s_uverejnenim_foto_video
      t.string :diagnoza
      t.boolean :platny_zaznam

      t.timestamps
    end
  end
end
