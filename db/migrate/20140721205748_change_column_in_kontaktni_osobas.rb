class ChangeColumnInKontaktniOsobas < ActiveRecord::Migration
  def change
  	change_column :kontaktni_osobas, :telefon, :string
  	change_column :kontaktni_osobas, :psc, :string
  	change_column :osobas, :telefon, :string
  	change_column :osobas, :psc, :string
  end
end
