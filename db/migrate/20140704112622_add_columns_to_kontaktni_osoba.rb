class AddColumnsToKontaktniOsoba < ActiveRecord::Migration
  def change
    add_column :kontaktni_osobas, :jmeno, :string
    add_column :kontaktni_osobas, :prijmeni, :string
    add_column :kontaktni_osobas, :email, :string
    add_column :kontaktni_osobas, :telefon, :integer
    add_column :kontaktni_osobas, :ulice, :string
    add_column :kontaktni_osobas, :cislo_popisne, :integer
    add_column :kontaktni_osobas, :cislo_orientacni, :integer
    add_column :kontaktni_osobas, :psc, :integer
    add_column :kontaktni_osobas, :mesto, :string
  end
end
