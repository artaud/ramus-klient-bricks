# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140722085359) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "friendly_id_slugs", force: true do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "histories", force: true do |t|
    t.string   "object_name"
    t.integer  "object_id"
    t.string   "attribute_name"
    t.string   "before_value"
    t.string   "after_value"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "histories", ["user_id"], name: "index_histories_on_user_id", using: :btree

  create_table "klient_leks", force: true do |t|
    t.integer  "klient_id"
    t.integer  "lek_id"
    t.float    "davkovani_rano"
    t.float    "davkovani_poledne"
    t.float    "davkovani_vecer"
    t.float    "davkovani_pred_spanim"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "klients", force: true do |t|
    t.integer  "osoba_id"
    t.string   "rodne_cislo"
    t.integer  "pojistovna"
    t.boolean  "vozickar"
    t.boolean  "epilepsie"
    t.text     "podoba_epileptickeho_zachvatu"
    t.text     "alergie"
    t.boolean  "inkontinence"
    t.text     "popis_projevu_diagnozy_a_instrukce_k_zachazeni_s_klientem"
    t.text     "denni_rezim"
    t.boolean  "plnoletost"
    t.text     "postrehy_ramus"
    t.boolean  "sklony_k_oddelovani_se_od_skupiny"
    t.boolean  "elektronicky_souhlas_s_uchovanim_udaju"
    t.boolean  "souhlas_s_uverejnenim_foto_video"
    t.string   "diagnoza"
    t.boolean  "platny_zaznam"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "prukaz"
  end

  create_table "klients_kontaktni_osobas", force: true do |t|
    t.integer  "klient_id"
    t.integer  "kontaktni_osoba_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "kontaktni_osobas", force: true do |t|
    t.integer  "osoba_id"
    t.integer  "klient_id"
    t.string   "vztah_ke_klientovi"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "jmeno"
    t.string   "prijmeni"
    t.string   "email"
    t.string   "telefon"
    t.string   "ulice"
    t.integer  "cislo_popisne"
    t.integer  "cislo_orientacni"
    t.string   "psc"
    t.string   "mesto"
  end

  create_table "leks", force: true do |t|
    t.string   "nazev"
    t.text     "pribalovy_letak"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "klient_id"
    t.text     "davkovani"
  end

  create_table "osobas", force: true do |t|
    t.string   "jmeno"
    t.string   "prijmeni"
    t.string   "email"
    t.string   "telefon"
    t.string   "ulice"
    t.integer  "cislo_popisne"
    t.integer  "cislo_orientacni"
    t.string   "psc"
    t.string   "mesto"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "kontaktni_osoba_id"
  end

  create_table "users", force: true do |t|
    t.string   "username"
    t.string   "email"
    t.string   "password_digest"
    t.boolean  "admin",           default: false
    t.string   "slug"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "osoba_id"
    t.datetime "last_seen_at"
  end

  add_index "users", ["slug"], name: "index_users_on_slug", unique: true, using: :btree

  create_table "zajem_o_informaces", force: true do |t|
    t.integer  "klient_id"
    t.boolean  "jemne_vikendovky"
    t.boolean  "drsne_vikendovky"
    t.boolean  "humr"
    t.boolean  "legendarni_tabory"
    t.boolean  "drsne_tabory"
    t.boolean  "rambar"
    t.boolean  "dilna"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
